﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

	private Transform cameraTransform;
	private Camera playerCamera;
	private int screenBuffer;
	// Use this for initialization
	void Start () {
		cameraTransform = GetComponent<Transform> ();
		playerCamera = GetComponent<Camera> ();
		if (Screen.height > Screen.width) {
			screenBuffer = (int)(0.01 * Screen.height);
		} else {
			screenBuffer = (int)(0.01 * Screen.width);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.mousePosition.x  >= 0 && Input.mousePosition.x <= (0+screenBuffer))) {
		//move screen to left
			Debug.Log("moving left");
			Vector3 left = new Vector3 (-1, 0, 0);
			cameraTransform.Translate (left, Space.World);
		}
		else if ((Input.mousePosition.y  >= 0 && Input.mousePosition.y <= (0+screenBuffer))) {
		//move screen down
			Debug.Log("moving down");
			Vector3 down = new Vector3 (0, 0, -1);
			cameraTransform.Translate (down, Space.World);
		}
		else if ((Input.mousePosition.x  <= Screen.width && Input.mousePosition.x >= (Screen.width-screenBuffer))) {
		//move screen right
			Debug.Log("moving right");
			Vector3 right = new Vector3 (1, 0, 0);
			cameraTransform.Translate (right, Space.World);
		}
		else if ((Input.mousePosition.y  <= Screen.height && Input.mousePosition.y >= (Screen.height-screenBuffer))) {
		//move screen up
			Debug.Log("moving up");
			Vector3 up = new Vector3 (0, 0, 1);
			cameraTransform.Translate (up, Space.World);
		}
		if (Input.mouseScrollDelta.y != 0) {
			playerCamera.fieldOfView = playerCamera.fieldOfView - Input.mouseScrollDelta.y;
		}
	}
}
