﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AstarAI : MonoBehaviour {
	public Vector3 targetPosition;

	public void Start() {
		Seeker seeker = GetComponent<Seeker> ();
		seeker.StartPath (transform.position, targetPosition, OnPathComplete);
	}

	public void OnPathComplete (Path p) {
		Debug.Log ("We got a path back. Did we get an error? : " + p.errorLog);
	}
}
